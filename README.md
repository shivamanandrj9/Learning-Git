# Name: 
Connector for Sprinklr-Salesforce Integration

# Description:
Medallia for Salesforce(https://www.medallia.com/xchange/medallia-for-salesforce/) provides certain features to its salesforce users. There does not exist direct APIs to do these things on Salesforce. Hence this application exposes APIs that can be used to have the same functionalities.

# Setting Up the Database

In resources/application.properties, add your mongodb connection string.<br>
spring.data.mongodb.uri=\<mongodb-connection-string\>

Also make sure your IP address is 

# API Endpoints:

## 1. Registering Salesforce Account With Application

1. Log in to your Salesforce org.
![Alt Text](DemoImages/1.png)

2. Go to the Setup page by clicking on the Setup link in the top-right corner of the Salesforce interface.

3. In the Setup page, search for "App Manager" in the quick find box and select "App Manager" from the search results.

4. On the App Manager page, click on the "New Connected App" button.

5. Fill in the following details in the connected app creation form:

    Name: Provide a name for your application.

    Contact Email: Enter the email address associated with the application.

    API (Enable OAuth Settings): Check the "Enable OAuth Settings" box.

    Callback URL: Enter the callback URL where users should be redirected after successful authentication. For example, you can use "https://login.salesforce.com/services/oauth2/callback" as the callback URL.

    Selected OAuth Scopes: Add the necessary OAuth scopes based on your application's requirements. For full access, you can select the "Full" scope.
    
    Click on the "Save" button, and then click on "Continue" to proceed.

6. After creating the connected app, go to the "Manage" section and find your app in the list. Click on the app name to access its details.

7. In the connected app details page, locate the "API (Enable OAuth Settings)" section and click on the "Click to reveal" link next to the "Consumer Secret" field. Make a note of the "Consumer Key" and "Consumer Secret".

8. In the same connected app details page, go to the "OAuth Policies" section. Under "IP Relaxation Policies," select "Relax IP restrictions."

9. Now make a POST request to the application

    ENDPOINT: /registerSalesforceAccount

    METHOD: POST

    BODY: 

        {
            "username":<User-Salesforce-Username>,
            "password":<User-Salesforce=Password>,
            "consumerKey":<ConsumerKey>,
            "consumerSecret":<ConsumerSecret>
        }

    EXPECTED RESPONSE:

        {
            "SalesforceToken":<Generated-Salesforce-Token>
        }

## 2. Saving The Credentials

DESCRIPTION: Saves credential to the database

ENDPOINT: /saveCredential

METHOD: POST

BODY: 

    {
        "username":<User-Salesforce-Username>,
        "password":<User-Salesforce=Password>,
        "consumerKey":<ConsumerKey>,
        "consumerSecret":<ConsumerSecret>
    }

## 3. Getting list of all the salesforce users

DESCRIPTION: Using the admin account of an organization, we can fetch all the users who are part of that organization.

ENDPOINT: /users

METHOD: GET

EXPECTED RESPONSE:

    [
        {
            <detail-of-user1>
        },
        {
            <detail-of-user2>
        }
    ]

## 4. Adding User To Group

DESCRIPTION: Add users to a group.

ENDPOINT: /invitationList/\<group-name\>/add

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY: 

    ["<username1>","<username2>"]


## 5. Deleting User From Group

DESCRIPTION: Delete users from a group.

ENDPOINT: /invitationList/\<group-name\>/remove

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY: 

    ["<username1>","<username2>"]

## 6. Deleting users from group

DESCRIPTION: Deleting users from a group.

ENDPOINT: /invitationList/\<group-name\>/remove

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: DELETE

BODY: 

    ["<username1>","<username2>"]


## 7. Fetching contact of users in a group

DESCRIPTION: Fetches email id of the users in a group.

ENDPOINT: /invitationList/\<group-name\>

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: GET

EXPECTED RESPONSE: 

    {
        "user-email1",
        "user-email2"
    }

## 8. Creating cases

DESCRIPTION: create case on salesforce

ENDPOINT: /create/case

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "AccountName":"<Account-associated-with-the-case>",
        "Status":"<case-status>",
        "Origin":"<case-origin>",
        "Subject":"<case-subject>",
        "Priority":"<case-priority>",
        "ParentCaseNumber":"<parent-case-number>"
    }

EXPECTED RESPONSE:

    {
        "Email": "<Email-of-person-associated-with-the-account>",
        "Subject": "<Status-of-the-case>
    }


## 9. Get all the cases

DESCRIPTION: Get all cases on salesforce.

ENDPOINT: /cases

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: GET

EXPECTED RESPONSE: 

    {
        "totalSize": <total-count-of-cases>,
        "done": <case-fetching-status>,
        "records": [
            {
                "Id": "<Salesforce-id-of-case>",
                "CaseNumber": "<Case-number-of-case>",
                "Subject": "<Subject-of-case>",
                "Status": "<Status-of-case>"
            }
        ]
    }

## 10. Updating status of cases

DESCRIPTION: Updates status of any case

ENDPOINT: /caseStatus

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: PATCH

BODY:

    {
        "CaseNumber": "<case-number>",
        "Status": "<status>"
    }

EXPECTED RESPONSE:

    {
        "Email": "<Email-of-person-associated-with-the-account>",
        "Status": "<Status-of-the-case>
    }

## 11. Creating followup of a case

DESCRIPTION: Creates follow-up of a case

ENDPOINT: /create/followupCases

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "AccountName":"<Account-associated-with-the-case>",
        "Status":"<case-status>",
        "Origin":"<case-origin>",
        "Subject":"<case-subject>",
        "Priority":"<case-priority>",
        "ParentCaseNumber":"<parent-case-number>"
    }

EXPECTED RESPONSE:

    {
        "Email": "<Email-of-person-associated-with-the-account>",
        "Status": "<Status-of-the-case>
    }

## 12. Creating orders on salesforce

DESCRIPTION: Creates order on salesforce account

ENDPOINT: /create/followupCases

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "AccountName":"<account-name>",
        "EffectiveDate":"<date-format-YY-MM-DD>",
        "Status":"<status>"
    }  

EXPECTED RESPONSE:

    {
        "Email": "<Email-of-person-associated-with-the-account>",
        "Status": "<Status-of-the-order>
    }

## 13. Get all the orders

DESCRIPTION: Get all orders on salesforce.

ENDPOINT: /orders

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: GET

EXPECTED RESPONSE: 

    {
        "totalSize": <total-number-of-orders>,
        "done": <fetching-status>,
        "records": [
            {
                "Id": "<id-of-order1>",
                "OrderNumber": "<order-number>",
                "Status": "<status-of-the-order>"
            }
        ]
    }

## 14. Sending chatter

DESCRIPTION: Sends chatter to the specified username

ENDPOINT: /send/chatter

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "Username":"<username-of-the-user-whom-we-are-sending-chatter>",
        "Message": "<message>"
    }


## 15. Register for automated notification

DESCRIPTION: 
Set automated notification for the client.<br>
The client on behalf of whom we are sending the feedback form to client's customer will be accepted in one of our database's collection. So we need to specify the Collection Name.<br>
The Field Name is used to identify the client's customer who has responded. Using this the automated chatter will go to the client that your customer has responded.

ENDPOINT: /send/notification

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "Collection Name":"<The-collection-in-your-database-in-which-you-are-going-to-accespt-feedback-responses-for-that-client>",
        "Field Name":"The-field-in-the-collection-which-stores-the-customer-who-reponded"
    }


## 16. Creating an object on salesforce

DESCRIPTION: We can create custom objects on salesforce using this endpoint. An object is similar to a schema in which we can describe the field name and their properties.

ENDPOINT: /create/object

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "ObjectName":"<Object-Name>",
        "NameField":"<Primary-Key-Field>",
        "Fields":[
            {
                <Field1 Specification>
            },
            {
                <Field2 Specification>
            }
        ]
    }


EXAMPLE:

    {
        "ObjectName":"DemoObject",
        "NameField":"EId",
        "Fields":[
            {
                "label":"EmployeeName",
                "type":"Text",
                "length":"120"
            },
            {
                "label":"Salary",
                "type":"Number",
                "precision":"7",
                "scale":"2"
            }
        ]
    }

Salesforce supports different type of fields 
check here : https://help.salesforce.com/s/articleView?id=sf.custom_field_types.htm&type=5


## 17. Add data to the object

DESCRIPTION: 
We can use this endpoint to add data of custom object type.

ENDPOINT: /add/\<Object-Name>

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "<Field1 Name>":"<Value1>",
        "<Field2 Name>":"<Value2>",
    }

EXAMPLE:

URL :- http://localhost:8080/object/add/DemoObject?Username=shivam.anand@sprinklr.com&Password=shivamanand64

BODY :-

    {
        "EId":"123",
        "EmployeeName":"Shivam",
        "Salary":5456
    }

## 18. Creating a folder

DESCRIPTION: 
We can create folders to store entities in salesforce.

ENDPOINT: /create/folder

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
        "Name": "<Name-of-the-dashboard>",
        "DeveloperName": "<Unique-name-to-identify-it>",
        "AccessType": "<Who-can-access-it>",
        "Type": "<The-type-of-content-you-are-going-to-store-in-it>"
    }

EXAMPLE:

POST REQUEST TO: http://localhost:8080/create/folder?Username=\<username\>&Password=\<password\>


    {
        "Name": "Demo Dashboards",
        "DeveloperName": "demo_dashboards",
        "AccessType": "Public",
        "Type": "Dashboard"
    }




EXAMPLE:

URL :- http://localhost:8080/object/add/DemoObject?Username=shivam.anand@sprinklr.com&Password=shivamanand64

BODY :-

    {
        "EId":"123",
        "EmployeeName":"Shivam",
        "Salary":5456
    }


# Dashboard APIs

Using different endpoints, we will see how to create a dashboard.

We will go step by step explaining the endpoints and example.<br>
For this demostration, we will use Starbuck's mock survey data.<br> 
Link:https://docs.google.com/spreadsheets/d/1XunREtil_3XmUmRI87WxVxRLUaKXbYTimAVm-lEkdyM/edit?usp=sharing


## Step 1:

We create a custom object on salesforce matching with the fields of our data using the endpoint discussed in point 16 above.

Example:-

URL :- http://localhost:8080/create/object?Username=\<username\>&Password=\<password\>

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>


BODY:-

    {
    "ObjectName":"StarbucksSurvey",
    "NameField":"Id",
    "Fields":[
        {
            "label":"Gender",
            "type":"Text",
            "length":"10"
        },
        {
            "label":"Age",
            "type":"Text",
            "length":"255"
        },
        {
            "label":"Status",
            "type":"Text",
            "length":"255"
        },
        {
            "label":"Income",
            "type":"Text",
            "length":"255"
        },
        {
            "label":"visitNo",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"Method",
            "type":"Text",
            "length":"255"
        },
        {
            "label":"averageTimeSpend",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"productRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"priceRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"promoRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"ambianceRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"wifiRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
         {
            "label":"serviceRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        },
        {
            "label":"chooseRate",
            "type": "Number",
            "precision":"4",
            "scale":"2"
        }
    ]
    }

## Step 2:

We need to push data of this custom object type to salesforce.

We can either use endpoint discussed in point 17 to upload data or you can directly upload your .csv file using the below endpoint.

ENDPOINT: /upload/\<custom-object-name\>

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>
    file: <file-address>

METHOD: POST

EXAMPLE:

URL: http://localhost:8080/upload/StarbucksSurvey?Username=\<username\>&Password=\<password\>&file=/Users/shivam/Downloads/Starbucks_Survey.csv

PARAMETERS:

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>
    file: /Users/shivam/Downloads/Starbucks_Survey.csv

## Step 3:

Now we create different types of report that we want to display on the dashboard using below endpoint.

ENDPOINT: /create/report

PARAMETER:

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:

    {
    "name": "<Report-Name>",
    "reportType": {
        "label": "<Name-of-the-custom-object-for-which-we-are-creating-report>",
        "type": "<Type-of-custom-object>"
    },
    "reportFormat": "<Format-of-the-report>",
    "detailColumns": [
        <names-of-the-columns-to-be-displayed-here>
    ],
    
    "folderId": "<folder-in-which-this-report-is-going-to-be-saved>",
    "groupingsAcross": [],
    "groupingsDown": [
        {
            <Specifies-the-column-on-basis-of-which-grouping-is-done>
        }
    ],
    
    "aggregates": [
        <names-of-different-aggregates-that-we-want-to-show>
    ],
    
    "customSummaryFormula": {
        <formula-description>
    }
    }

EXAMPLE:-

    {
    //name of the report
    "name": "Starbuck's Product Rating",
    //type of report
    "reportType": {
        "label": "StarbucksSurveys",
        "type": "CustomEntity$StarbucksSurvey__c"
    },
    "reportFormat": "SUMMARY",
    //this should not include the one which are grouped
    "detailColumns": [
        "CUST_NAME"
    ],
    //folder
    "folderId": "00l5i000002AQDZAA4",
    "groupingsAcross": [],
    "groupingsDown": [
        {
            "dateGranularity": "None",
            "name": "CUST_CREATED_NAME",
            "sortAggregate": null,
            "sortOrder": "Asc"
        }
    ],
    //these will be shown in report
    "aggregates": [
        "FORMULA1",
        "RowCount"
    ],
    //formula description
    "customSummaryFormula": {
        "FORMULA1": {
            "acrossGroup": null,
            "acrossGroupType": "all",
            "decimalPlaces": 2,
            "description": null,
            "downGroup": null,
            "downGroupType": "all",
            "formula": "StarbucksSurvey__c.productRate__c:AVG",
            "formulaType": "number",
            "label": "Average Product Rating"
        }
    }
    }


## Step 4:


Once Such reports are ready, it's time to show them on dashboard but first we need to create an empty dashboard using the below API call.


DESCRIPTION: 
We can use this to create dashboard.

ENDPOINT: /create/dashboard

PARAMETERS: 

    Username: <Salesforce-Account-Username>
    Password: <Salesforce-Account-Password>

METHOD: POST

BODY:



EXAMPLE:

URL :- http://localhost:8080/object/add/DemoObject?Username=shivam.anand@sprinklr.com&Password=shivamanand64

BODY :-

    {
        "EId":"123",
        "EmployeeName":"Shivam",
        "Salary":5456
    }




















